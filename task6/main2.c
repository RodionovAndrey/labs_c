#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#define rankA 2
int main(int argc, char *argv[]) {
    int i, pid[rankA], status, stat;
    int a[]={1 ,4};
    int b[2];
    int A[2][2]={4, 5, 8, 3};
    FILE *popen(const char *command, const char *type); 
    for (i = 0; i < rankA; i++) { // создаем процессы для всех строк матрицы А
        // запускаем дочерний процесс 
        pid[i] = fork();
        if (-1 == pid[i]) {
            perror("fork"); /* произошла ошибка */
            exit(1); /*выход из родительского процесса*/
        } else if (0 == pid[i]) {
            printf(" CHILD: Это %d процесс-потомок СТАРТ!\n", i);
            sleep(rand() % 4);
            for (int j = 0; j < rankA; ++j)
            {
            	b[j]=a[0]*A[j][0]+a[1]*A[j][1];

            }
            printf(" CHILD: Это %d процесс-потомок ВЫХОД!\n", i);
            exit(b[i]); /* выход из процесс-потомока */
        }
    }
    // если выполняется родительский процесс
    printf("PARENT: Это процесс-родитель!\n");
    printf("Матрица А\n");
    for (int i = 0; i < rankA; ++i)
    {
        for (int j = 0; j < rankA; ++j)
        {
            printf("%d ", A[i][j]);
        }
        printf("\n");
    }
    printf("Вектор а\n");
    for (int i = 0; i < rankA; ++i)
    {
        printf("%d ", a[i]);
    }
    printf("\n");
    printf("b=A*a\n");
    // ожидание окончания выполнения всех запущенных процессов
    for (i = 0; i < rankA; i++) {
        status = waitpid(pid[i], &stat, 0);
        if (pid[i] == status) {
            printf("процесс-потомок %d done, b[%d]=%d\n", i, i, WEXITSTATUS(stat));
        }
    }
    return 0;
}