//--- threads2.c
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <pthread.h>
#define N 3 // количество самолетов-разведчиков
#define rankA 10 // размерность поля
#define dens 3 //плотность заполнения карты целями, 3=30%

int A[rankA][rankA]; // глобальная переменная поля для разведки

void seedA(int Arr[rankA][rankA]){
    srand(time(NULL));
    int prob=0;
    for ( int i = 0; i < rankA; i ++){   
        for ( int j = 0; j <rankA; j ++){      
            prob = rand ()%rankA; // создаем случайное число от 0 до 10
            if(prob<=dens){
            	Arr [i][j] = 1; 
            }else{
            	Arr [i][j] = 0;
            }
            printf("%d ", Arr [i][j]);
        }
        printf("\n");
    }
}

void *scout(void *arg){
	srand(time(NULL)+pthread_self()); 
	int targets = * (int *) arg;
	targets = 0;
	int *ps = (int*)malloc(sizeof(int)); 
	int xm=0, ym=0; // траектория движения
	int x = rand ()%(rankA-1), y = rand ()%(rankA-1); //координаты рожденного процесса; //координаты рожденного процесса   
	printf("Разведчик появился на x=%d y=%d \n", x,y);        
        if( x-1>=rankA-y && x-1>=rankA-x && x-1>=y-1){
            ym=0; xm=-1;
            printf("Разведчик решил лететь налево\n");
        }
        else if( rankA-y>=rankA-x && rankA-y>=y-1 && rankA-y>=x-1){
            ym=1; xm=0;
            printf("Разведчик решил лететь вниз\n");
        }
        else if( rankA-x>=rankA-y && rankA-x>=y-1 && rankA-x>=x-1){
            ym=0; xm=1;
            printf("Разведчик решил лететь направо\n");
        }
        else if( y-1>=rankA-y && y-1>=rankA-x && y-1>=x-1){
            ym=-1; xm=0;
            printf("Разведчик решил лететь вверх\n");
        }
    
    while((x>0 && x<rankA) && (y>0 && y<rankA)){
    	if(A[x][y]==1){
    		targets++;
    	}
    	x=x+xm;
    	y=y+ym;
    }
    *ps=targets;
    printf("Обнаружено целей: %d\n\n", targets);
    pthread_exit((void*)ps);
}

int main(int argc, char const *argv[])
{
	//int A[rankA][rankA];
	seedA(A);
	//scout(A);

	int result;
	pthread_t threads[N];
	int targ[N];
	void *status[N];

	for (int i = 0; i < N; i++){
		result = pthread_create(&threads[i], NULL, scout, &targ[i]);
		printf("Разведчик %lu\n", threads[i]);
		sleep(1);
		if (result != 0) {
			perror("Creating the first thread");
			return EXIT_FAILURE;
		}	
	}
	
	printf("Тест доступа к количествам целей вне функций\n");
	for (int i = 0; i < N; i++){
		result = pthread_join(threads[i], &status[i]);
		if (result != 0) {
			perror("Joining the first thread");
			return EXIT_FAILURE;
		} else {
			printf("Количество обнаруженных целей разведчиком %lu =%d\n", threads[i], *((int*)status[i]));
		}
		free(status[i]);
	}

	return 0;
}