#include <stdio.h>
#include <stdlib.h>
#include <mcheck.h> /* для mtrace */
#include <string.h>
#define MAX_LEN 1024 /* максимальная длина строки */

struct point { //создание структур: фамилия, группа, место прохождения практики, оценка
    		char surname[MAX_LEN]; //фамилия
    		char prPlace[MAX_LEN]; //место прохождения практики
    		int gr; //группа
    		int mark; // оценка
	} pt1;

int rstudNUM(){
	int studNUM = 0;   // количество участников списка
	scanf("%d",	&studNUM);
	return studNUM;
}

struct point* readStruct(int studNUM){
 	//studNUM = 0;   // количество участников списка
	//scanf("%d",	&studNUM);
	struct point *mas; //указатель на массив структур
	mas = (struct point*)malloc(sizeof(pt1)*studNUM); // указатель на массив 
	//void readStruct(struct point,)
	for (int i = 0; i < studNUM; ++i)
	{
		scanf("%s%s%d%d", pt1.surname, pt1.prPlace, &pt1.gr, &pt1.mark);
		//printf("%s%c%s%c%d%c%d\n", pt1.surname,' ', pt1.prPlace,' ', pt1.gr,' ', pt1.mark);
		//mas = (struct point*)malloc(sizeof(pt1)*studNUM);
		mas[i]=pt1;
	} 
return mas;
}

void printStruct(int studNUM, struct point *mas){ //функция принимает количество человек в списке и указатель на массив со списком
	for (int i = 0; i < studNUM; ++i)
	{
		printf("%s%c%s%c%d%c%d\n", mas[i].surname,' ', mas[i].prPlace,' ', mas[i].gr,' ', mas[i].mark);
	} 
}

int structcmp(const void *a, const void *b){
	
	struct point *pt1=(struct point *)a;
	struct point *pt2=(struct point *)b;
	return (pt2->mark - pt1->mark);
} 

int main(int argc, char const *argv[]){
  	int studNUM = 0;
  	struct point *p;
  	studNUM=rstudNUM(); // прочитали переменную с количеством человек в списке
  	p=readStruct(studNUM); //прочитали список и присвоили адрес массива p
  	printf("Фамилий в списке:\n");
  	printf("%d\n", studNUM);
  	printf("\n Список:\n");
  	printStruct(studNUM, p);
  	//s=p[1];
	//readStruct(0);
	printf("\n После сортировки:\n");	
	qsort(p, studNUM, sizeof(pt1), structcmp);
	printStruct(studNUM, p);
	return 0;
}



