//входных параметров нет, все захардкожено

#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for socket(), connect(), sendto(), and recvfrom() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_addr() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */
#include <netinet/in.h>
#include <netdb.h> 
#include <pthread.h>

#include <wait.h>

#define MAXRECVSTRING 255  /* Longest string to receive */
#define MAXLEN 10 // максимальная длина строки
#define sleepTCP 1 // сон после отправки TCP пакета
#define portno 5002 // TCP порт 
#define broadcastPort 5001 // UDP порт 
#define T1 1 //мин время обработки сообщения
#define T2 1 //Tmax=T1+T2 время обработки сообщения
#define broadcastIP "127.0.0.1" 
void error(const char *msg)
{
    perror(msg);
    exit(0);
}

static char *rand_string(char *str, size_t size)
{
    const char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJK...";
    if (size) {
        --size;
        for (size_t n = 0; n < size; n++) {
            int key = rand() % (int) (sizeof charset - 1);
            //printf("%d\n", key);
            str[n] = charset[key];
        }
        str[size] = '\0';
    }
    return str;
}

char* rand_string_alloc(size_t size){
         char *s = malloc(size + 1);
         if (s) {
             rand_string(s, size);
         }
         return s;
    }
int main(int argc, char *argv[])
{
    sigset_t set;
    sigemptyset( &set );
    sigaddset( &set, SIGUSR1);
    sigaddset( &set, SIGUSR2);
    sigprocmask(SIG_BLOCK, &set, NULL); // Разобраться зачем это нужно, без этой строчки не работает                                              
    int sig;
    int child_pid[1]={0};
    int parent_pid=0, pid[1]={0};
    parent_pid=getpid();

    ////////////////////////////////////////////////////////////////////// создаем прослушку UDP
    int UDPsockcl12;                         /* Socket */
    struct sockaddr_in broadcastAddr; /* Broadcast Address */
    socklen_t fromSize;
    char recvString[MAXRECVSTRING+1]; /* Buffer for received string */
    int recvStringLen;                /* Length of received string */
    char* strin;
    strin="Waiting";

    /* Create a best-effort datagram socket using UDP */
    if ((UDPsockcl12 = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
        error("socket() failed");

    int optval = 1;
    if (setsockopt(UDPsockcl12, SOL_SOCKET, SO_REUSEPORT, &optval, sizeof(optval))<0)
        error("setsockopt() failed");

    /* Construct bind structure */
    memset(&broadcastAddr, 0, sizeof(broadcastAddr));   /* Zero out structure */
    broadcastAddr.sin_family = AF_INET;                 /* Internet address family */
    broadcastAddr.sin_addr.s_addr = htonl(INADDR_ANY);  /* Any incoming interface */
    //broadcastAddr.sin_addr.s_addr = inet_addr(broadcastIP);
    broadcastAddr.sin_port = htons(broadcastPort);      /* Broadcast port */
    fromSize = sizeof(broadcastAddr);
    /* Bind to the broadcast port */
    if (bind(UDPsockcl12, (struct sockaddr *) &broadcastAddr, sizeof(broadcastAddr)) < 0)
      error("bind() failed");

    /////////////////////////////////////////////////////////////////////готовы получать UDP 
    pid[0] = fork();
        if(pid[0]>0){
            child_pid[0]=pid[0];
         } 
        if (-1 == pid[0]) {
            perror("fork"); // произошла ошибка 
            exit(1); //выход из родительского процесса
        } else if (0 == pid[0]) {
                int tmp=0;
                for(;;){
                    if(tmp==0){
                    	printf("Я клиент 1-2 \n");
                        printf("Жду UDP пакета \"Waiting\"...");
                        fflush(stdout);
                    }
                    //recvString="aaaaa";
                    /*if (sendto(UDPsockcl12, strin, sizeof(strin), 0, (struct sockaddr *) &broadcastAddr, sizeof(broadcastAddr)) <0){
                    error("sendto() sent a different number of bytes than expected");
                    }

                    printf("Отправил UDP\n");*/
                    if((recvStringLen = recvfrom(UDPsockcl12, recvString, MAXRECVSTRING, 0, (struct sockaddr *) &broadcastAddr, &fromSize)) <0)
                       error("recvfrom() failed");
                    recvString[recvStringLen] = '\0';
                    if(strcmp(strin,recvString)==0){
                        if(tmp==0){
                            printf("Получено. Дальнейшие UDP уведомления скрыты.\n \n");
                            tmp=1;
                        }
                        kill(parent_pid, SIGUSR1);
                    }else{printf("Сообщение не для меня \n");}

                }
                kill(parent_pid, SIGUSR2);
            exit(0); //выход из процесс-потомока 
        }
    
    /////////////////////////////////////////////////////////////////////

    //printf("PARENT: Это процесс-родитель!
    
    sigwait( &set, &sig); // ждем сообщения
    
        int TCPsockcl12;
        struct sockaddr_in serv_addr;
        struct hostent *server;

        // Шаг 1 - создание сокета
        TCPsockcl12 = socket(AF_INET, SOCK_STREAM, 0);
        if (TCPsockcl12 < 0) 
            error("ERROR opening socket");
        // извлечение хоста
        server = gethostbyname("127.0.0.1");
        if (server == NULL) {
            fprintf(stderr,"ERROR, no such host\n");
            exit(0);
        }
        // заполенние структуры serv_addr
        bzero((char *) &serv_addr, sizeof(serv_addr));
        serv_addr.sin_family = AF_INET;
        bcopy((char *)server->h_addr, 
             (char *)&serv_addr.sin_addr.s_addr,
             server->h_length);
        // установка порта
        serv_addr.sin_port = htons(portno);
        
        // Шаг 2 - установка соединения 
        if (connect(TCPsockcl12,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
            error("ERROR connecting");
    
    for(;;){
        sigwait( &set, &sig); // ждем сообщения
        sleep(3);
        char* randstr;
        srand(time(NULL));
        int t = rand()%5+5;
        randstr=rand_string_alloc(t); 
        printf("Отправляю TCP сообщение серверу: %10s\n", randstr);
        if(send(TCPsockcl12, randstr, strlen(randstr)+1, 0)<0){
            error("Ошибка при отправке сообщения");
        }
        sleep(rand()%T1+T2); 
        //exit(0);
        //printf("Предположим я послал TCP пакет\n");
    }
    close(TCPsockcl12);
    close(UDPsockcl12);
    exit(0);
}

