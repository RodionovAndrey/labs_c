//#include <wait.h>
//#include <fcntl.h> 
#include <stdio.h> /* for printf() and fprintf() */
#include <unistd.h> /* for close() */
#include <string.h> /* for memset() */
//#define _XOPEN_SOURCE 500
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <stdlib.h> /* for atoi() and exit() */
#include <sys/ipc.h>
#include <sys/msg.h>
#include <arpa/inet.h>  /* for sockaddr_in and inet_addr() */
//#include <sys/errno.h>
//#include <time.h>  // для time(NULL)
//#include <sys/socket.h> /* for socket(), connect(), sendto(), and recvfrom() */
//#include <netinet/in.h>
//#include <netdb.h> 
/*          константы         */

/****************************************/

#define SIZE_QUEUE_ARRAY 3  //Максимальный размер очереди
#define MAXRECV 10           //Максимальный размер принимаемого сообщения
#define broadcastPort1 5001 // порт для сообщений UDP клиентам 1ого типа
#define broadcastPort2 5000 // порт для сообщений UDP клиентам 2ого типа
#define broadcastIP "127.0.0.1" 
#define portno0 5002 // TCP порт для клиентов 1ого типа
#define portno1 5003 // TCP порт для клиентов 2ого типа
#define K 2 //интервал отправки UDP "Waiting"
#define L 4 //интервал отправки UDP "Ready"

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

struct mymsgbuf {
        long mtype;
        char str[MAXRECV]; // игровое поле
};

int msgqid, rc;

void send_message(int qid, struct mymsgbuf *qbuf, long type){
        qbuf->mtype = type;
        if((msgsnd(qid, (struct mymsgbuf *)qbuf,sizeof(struct mymsgbuf)-sizeof(long), 0)) ==-1){
                perror("msgsnd");
                exit(1);
        }
}

void read_message(int qid, struct mymsgbuf *qbuf, long type){
        qbuf->mtype = type;
        msgrcv(qid, (struct mymsgbuf *)qbuf, sizeof(struct mymsgbuf)-sizeof(long), type, 0);
        //printf("Points: %ld My var: %d TextLen: %s\n", qbuf->points, qbuf->ma, qbuf->mtext);
}

int main() {
    /*окружение для UDP*/
    int UDPsockServ1, UDPsockServ2;                         /* Socket */
    struct sockaddr_in broadcastAddr1, broadcastAddr2; /* Broadcast address */
    //char *broadcastIP;                /* IP broadcast address */
    char *sendString1, *sendString2;                 /* String to broadcast */
    int broadcastPermission;          /* Socket opt to set permission to broadcast */
    unsigned int sendStringLen1, sendStringLen2;       /* Length of string to broadcast */

    //broadcastIP = "127.0.0.1";            /* First arg:  broadcast IP address */ 
    sendString1="Waiting";
    sendString2="Ready";
    sendStringLen1=strlen(sendString1);
    sendStringLen2=strlen(sendString2);
    /* Create socket for sending/receiving datagrams */
    if ((UDPsockServ1 = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
        error("socket() failed");
    if ((UDPsockServ2 = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
        error("socket() failed");

    /* Set socket to allow broadcast */
    broadcastPermission = 1;
    if (setsockopt(UDPsockServ1, SOL_SOCKET, SO_BROADCAST, (void *) &broadcastPermission, 
          sizeof(broadcastPermission)) < 0)
        error("setsockopt() failed");

    if (setsockopt(UDPsockServ2, SOL_SOCKET, SO_BROADCAST, (void *) &broadcastPermission, 
          sizeof(broadcastPermission)) < 0)
        error("setsockopt() failed");

    /* Construct local address structure for CL1*/
    memset(&broadcastAddr1, 0, sizeof(broadcastAddr1));   /* Zero out structure */
    broadcastAddr1.sin_family = AF_INET;                 /* Internet address family */
    //broadcastAddr1.sin_addr.s_addr = inet_addr(broadcastIP);/* Broadcast IP address */
    broadcastAddr1.sin_addr.s_addr = htonl(INADDR_ANY);
    broadcastAddr1.sin_port = htons(broadcastPort1);         /* Broadcast port */
    /* Construct local address structure for CL2*/
    memset(&broadcastAddr2, 0, sizeof(broadcastAddr2));   /* Zero out structure */
    broadcastAddr2.sin_family = AF_INET;                 /* Internet address family */
    broadcastAddr2.sin_addr.s_addr = inet_addr(broadcastIP);/* Broadcast IP address */
    broadcastAddr2.sin_port = htons(broadcastPort2);         /* Broadcast port */
    //binding for clients 1
    /*if (bind(UDPsockServ1, (struct sockaddr *) &broadcastAddr1, sizeof(broadcastAddr1)) < 0)
        error("bind() failed");*/
    
    /*Окружение для очереди*/
    key_t key;
    int qtype = 1; 
    struct msqid_ds buff;
    //msgctl(msgqid, IPC_RMID, &buff);
    //printf("Длина очереди %lu\n", buff.msg_qnum);
    key = ftok(".", 'm');
    if((msgqid = msgget(key, IPC_CREAT|0660)) == -1) {
        perror("msgget");
        exit(1);
    }
    //msgctl(msgqid, IPC_RMID, &buff);
    //msgctl(msgqid, IPC_STAT, &buff); // обновляем информацию о очереди
    struct mymsgbuf qbuf;
    //printf("Длина очереди %lu\n", buff.msg_qnum);
    /*Окружение для сигналов*/
    sigset_t set;
    sigemptyset( &set );
    sigaddset( &set, SIGUSR1);
    sigaddset( &set, SIGUSR2);
    sigprocmask(SIG_BLOCK, &set, NULL); // Разобраться зачем это нужно, без этой строчки не работает                                              
    int sig;
    int child_pid[2]={0, 0};
    int parent_pid=0, pid[2]={0,0}, pid0;
    parent_pid=getpid();

    /*Окружение для TCP*/
    int TCPsockServ1, TCPsockServ2, newsockfd; // дескрипторы сокетов
    
    struct sockaddr_in serv_addr0, serv_addr1, cli_addr0, cli_addr1; // структура сокета сервера и клиента
    char msgBuf[MAXRECV]; //буфер для хранения сообщения

    for (int i = 0; i < 2; i++) { 
        pid[i] = fork();
        if(pid[i]>0){
            child_pid[i]=pid[i];
         } 
        if (-1 == pid[i]) {
            perror("fork"); // произошла ошибка 
            exit(1); //выход из родительского процесса
        } else if (0 == pid[i]) { //////////////создание TCP подключения для клиентов типа 1, который присылают TCP
            if(i==0){
                socklen_t clilen; // размер адреса клиента типа socklen_t
                
                // Шаг 1 - создание сокета
                    TCPsockServ1 = socket(AF_INET, SOCK_STREAM, 0);
                // ошибка при создании сокета
                    if (TCPsockServ1 < 0) 
                       error("ERROR opening socket");
                 
                // Шаг 2 - связывание сокета с локальным адресом
                    bzero((char *) &serv_addr0, sizeof(serv_addr0));
                    serv_addr0.sin_family = AF_INET;
                    serv_addr0.sin_addr.s_addr = INADDR_ANY; // сервер принимает подключения на все IP-адреса
                    serv_addr0.sin_port = htons(portno0);
                // вызываем bind для связывания
                    if (bind(TCPsockServ1, (struct sockaddr *) &serv_addr0, sizeof(serv_addr0)) < 0) 
                              error("ERROR on binding i=0");
                // Шаг 3 - ожидание подключений, размер очереди - 5
                    listen(TCPsockServ1,5);//////////////////////////////////начато ожидание TCP пакета
                    clilen = sizeof(cli_addr0);
                    
                // Шаг 4 - извлекаем сообщение из очереди
                // цикл извлечения запросов на подключение из очереди    
                    while (1) {
                        newsockfd = accept(TCPsockServ1,(struct sockaddr *) &cli_addr0, &clilen);
                        if (newsockfd < 0) 
                        error("ERROR on accept");
                        //printf("ОЖидаю TCP сообщение клиента1\n");

                        pid0 = fork();
                        if (pid < 0)
                        error("ERROR on fork");
                        if (pid0 == 0){
                            close(TCPsockServ1);
                            for(;;){
                                do{
                                    sigwait( &set, &sig);
                                }while(sig!=SIGUSR1);
                                //int k=0;
                                if(recv(newsockfd, msgBuf, MAXRECV, 0)<0){
                                    error("Ошибка при получении TCP пакета клиенту 1");
                                }
                                strcpy(qbuf.str,msgBuf);
                                msgctl(msgqid, IPC_STAT, &buff);
                                printf("Получено сообщение от клиента 1-ого типа: %10s, длина очереди %lu \n", qbuf.str, buff.msg_qnum);
                                send_message(msgqid, (struct mymsgbuf *)&qbuf, qtype);
                            }
                            exit(0);
                        }else{
                            close(newsockfd);
                        }
                    }
                    printf("Сюда не должны попасть\n");
                    close(TCPsockServ1);//первый процесс отвечает за связь с Клиентом 1
            }else if(i==1){
                socklen_t clilen1; // размер адреса клиента типа socklen_t
                
                // Шаг 1 - создание сокета
                    TCPsockServ2 = socket(AF_INET, SOCK_STREAM, 0);
                // ошибка при создании сокета
                    if (TCPsockServ2 < 0) 
                       error("ERROR opening socket");
                 
                // Шаг 2 - связывание сокета с локальным адресом
                    bzero((char *) &serv_addr1, sizeof(serv_addr1));
                    serv_addr1.sin_family = AF_INET;
                    serv_addr1.sin_addr.s_addr = INADDR_ANY; // сервер принимает подключения на все IP-адреса
                    serv_addr1.sin_port = htons(portno1);
                // вызываем bind для связывания
                    if (bind(TCPsockServ2, (struct sockaddr *) &serv_addr1, sizeof(serv_addr1)) < 0) 
                              error("ERROR on binding i=1");
                // Шаг 3 - ожидание подключений, размер очереди - 5
                    listen(TCPsockServ2,5);//////////////////////////////////начато ожидание TCP пакета
                    clilen1 = sizeof(cli_addr1);
                    
                // Шаг 4 - извлекаем сообщение из очереди
                // цикл извлечения запросов на подключение из очереди    
                    while (1) {
                        newsockfd = accept(TCPsockServ2,(struct sockaddr *) &cli_addr1, &clilen1);
                        if (newsockfd < 0) 
                        error("ERROR on accept");
                        //printf("ОЖидаю TCP сообщение клиента1\n");

                        pid0 = fork();
                        if (pid < 0)
                        error("ERROR on fork");
                        if (pid0 == 0){
                            close(TCPsockServ2);
                            for(;;){
                                do{
                                    sigwait( &set, &sig);
                                }while(sig!=SIGUSR2);
                                read_message(msgqid, (struct mymsgbuf *)&qbuf, qtype);
                                msgctl(msgqid, IPC_STAT, &buff); 
                                printf("Отправляю сообщение клиенту 2-ого типа:   %10s, длина очереди %lu \n", qbuf.str, buff.msg_qnum);
                                if(send(newsockfd, qbuf.str, MAXRECV, 0)<0){
                                    error("Ошибка при получении TCP пакета клиенту 1");
                                }
                            }
                            exit(0);
                        }else{
                            close(newsockfd);
                        }
                    }
                    printf("Сюда не должны попасть\n");
                    close(TCPsockServ2);//первый процесс отвечает за связь с Клиентом 1
                }
            }
    }
    
    //printf("PARENT: Это процесс-родитель!
    printf("Я СЕРВЕР\n");
    pid0 = fork();
    if (pid < 0)
        error("ERROR on fork");
    if (pid0 == 0){
        for(;;){
            msgctl(msgqid, IPC_STAT, &buff);
            if(buff.msg_qnum<SIZE_QUEUE_ARRAY){
                kill(0, SIGUSR1);
                //printf("Сигнал отправлен пиду %d\n", child_pid[0]);
                msgctl(msgqid, IPC_STAT, &buff);
                printf("\"Waiting\"... В очереди %lu\n", buff.msg_qnum);
                fflush(stdout); 
                /*socklen_t fromSize; 
                fromSize=sizeof(broadcastAddr1);
                char p="Waitinf";
                if((recvfrom(UDPsockServ1, p, 8, 0, (struct sockaddr *) &broadcastAddr1, &fromSize)) <0)
                       error("recvfrom() failed");
                   printf("Получил что то\n");*/

                if (sendto(UDPsockServ1, sendString1, sendStringLen1, 0, (struct sockaddr *) &broadcastAddr1, sizeof(broadcastAddr1)) != sendStringLen1){
                    error("sendto() sent a different number of bytes than expected");
                }
                sleep(K);
                }
        }
        exit(0);
    }
    for(;;){    
        msgctl(msgqid, IPC_STAT, &buff);
        if(buff.msg_qnum>0){
            kill(0, SIGUSR2);
           // printf("Сигнал отправлен пиду %d\n", child_pid[0]);
            msgctl(msgqid, IPC_STAT, &buff);
            printf("\"Ready\"... В очереди %lu\n", buff.msg_qnum);
            fflush(stdout); 
            if (sendto(UDPsockServ2, sendString2, sendStringLen2, 0, (struct sockaddr *) &broadcastAddr2, sizeof(broadcastAddr2)) != sendStringLen2){
             error("sendto() sent a different number of bytes than expected");
            }
            sleep(L);
        }
    }
    return 0;
}



