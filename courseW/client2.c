//входных параметров нет, все захардкожено

#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for socket(), connect(), sendto(), and recvfrom() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_addr() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */
#include <netinet/in.h>
#include <netdb.h> 
#include <pthread.h>

#include <wait.h>

#define MAXRECVSTRING 255  /* Longest string to receive */
#define MAXLEN 10 // максимальная длина строки
#define sleepTCP 1 // сон после отправки TCP пакета
#define MAXRECV 10
#define portno 5003 // TCP порт 
#define broadcastPort 5000 // UDP порт 
#define T1 1 //Tmin обработки сообщения
#define T2 1 //Tmax=T1+T2 

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

void randstr(int my_sock2);


int main(int argc, char *argv[])
{
    sigset_t set;
    sigemptyset( &set );
    sigaddset( &set, SIGUSR1);
    sigaddset( &set, SIGUSR2);
    sigprocmask(SIG_BLOCK, &set, NULL); // Разобраться зачем это нужно, без этой строчки не работает                                              
    int sig;
    int child_pid[1]={0};
    int parent_pid=0, pid[1]={0};
    parent_pid=getpid();

    ////////////////////////////////////////////////////////////////////// создаем прослушку UDP
    int UDPsockcl2, TCPsockcl2;                         /* Socket */
    struct sockaddr_in broadcastAddr; /* Broadcast Address */
    char recvString[MAXRECVSTRING+1]; /* Buffer for received string */
    int recvStringLen;                /* Length of received string */
       /* First arg: broadcast port */
    char* strin;
    strin="Ready";

    /* Create a best-effort datagram socket using UDP */
    if ((UDPsockcl2 = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
        error("socket() failed");

     int optval = 1;
    if (setsockopt(UDPsockcl2, SOL_SOCKET, SO_REUSEPORT, &optval, sizeof(optval))<0)
        error("setsockopt() failed");

    /* Construct bind structure */
    memset(&broadcastAddr, 0, sizeof(broadcastAddr));   /* Zero out structure */
    broadcastAddr.sin_family = AF_INET;                 /* Internet address family */
    broadcastAddr.sin_addr.s_addr = htonl(INADDR_ANY);  /* Any incoming interface */
    broadcastAddr.sin_port = htons(broadcastPort);      /* Broadcast port */

    /* Bind to the broadcast port */
    if (bind(UDPsockcl2, (struct sockaddr *) &broadcastAddr, sizeof(broadcastAddr)) < 0)
        error("bind() failed");
    /////////////////////////////////////////////////////////////////////готовы получать UDP 
    pid[0] = fork();
        if(pid[0]>0){
            child_pid[0]=pid[0];
         } 
        if (-1 == pid[0]) {
            perror("fork"); // произошла ошибка 
            exit(1); //выход из родительского процесса
        } else if (0 == pid[0]) {
                int tmp=0;
                for(;;){
                    if(tmp==0){
                        printf("Я клиент 2-1 \n");
                        printf("Жду UDP пакета \"Ready\"...");
                        fflush(stdout);
                    }
                    if ((recvStringLen = recvfrom(UDPsockcl2, recvString, MAXRECVSTRING, 0, NULL, 0)) < 0)
                        error("recvfrom() failed");
                    recvString[recvStringLen] = '\0';
                    if(strcmp(strin,recvString)==0){
                        if(tmp==0){
                            printf("Получено. Дальнейшие UDP уведомления скрыты.\n \n");
                            tmp=1;
                        }
                        kill(parent_pid, SIGUSR1);
                    }else{printf("Сообщение не для меня \n");}
                }
                kill(parent_pid, SIGUSR2);
            exit(0); //выход из процесс-потомока 
        }
    
    /////////////////////////////////////////////////////////////////////

    //printf("PARENT: Это процесс-родитель!
    
        struct sockaddr_in serv_addr;
        struct hostent *server;
        
        // Шаг 1 - создание сокета
        TCPsockcl2 = socket(AF_INET, SOCK_STREAM, 0);
        if (TCPsockcl2 < 0) 
            error("ERROR opening socket");
        // извлечение хоста
        server = gethostbyname("127.0.0.1");
        if (server == NULL) {
            fprintf(stderr,"ERROR, no such host\n");
            exit(0);
        }
        // заполенние структуры serv_addr
        bzero((char *) &serv_addr, sizeof(serv_addr));
        serv_addr.sin_family = AF_INET;
        bcopy((char *)server->h_addr, 
             (char *)&serv_addr.sin_addr.s_addr,
             server->h_length);
        // установка порта
        serv_addr.sin_port = htons(portno);
        sigwait( &set, &sig); //ОЖИдаем сообщения что UDP пакет получен
        // Шаг 2 - установка соединения 
        if (connect(TCPsockcl2,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
            error("ERROR connecting");
    
    for(;;){
        char msgBuf[MAXRECV]; //буфер для хранения сообщения
        if(recv(TCPsockcl2, msgBuf, MAXRECV, 0)<0){
            error("Ошибка при получении TCP пакета");
        }
    printf("Получил TCP сообщение от сервера %10s\n", msgBuf);
    sleep(rand()%T1+T2); 
    }
    close(TCPsockcl2);
    close(UDPsockcl2);
    exit(0);
}

